package com.parra.googlemapskeysenati;

import androidx.fragment.app.FragmentActivity;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnMarkerDragListener {

    //mapear el LOG
    public static final String TAG = MapsActivity.class.getSimpleName();

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG,">>>onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        //mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

        // Add a marker in Sydney and move the camera - es el punto donde te encuentras en el maps
        //LATITUD: -12.062110064688014
        //LONGITUD: -77.03654050827028


        //Click sobre el Marker
        mMap.setOnMarkerClickListener(this);
        //Click sobre el Map
        mMap.setOnMapClickListener(this);

        //Click sobre el DRAG
        mMap.setOnMarkerDragListener(this);

        LatLng lima = new LatLng(-12.062110064688014, -77.03654050827028);
        mMap.addMarker(new MarkerOptions().position(lima).title("Estoy en Lima").snippet("Es mi lugar favorito").draggable(true)
        .icon(BitmapDescriptorFactory.fromResource(R.drawable.nazca_lines)));

        //Personalizar estilos al mapa
        try{
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");}
            }catch(Resources.NotFoundException e){
                Log.e(TAG, "Can't find style. Error: ", e);
            }
        mMap.moveCamera(CameraUpdateFactory.newLatLng(lima));



        LatLng francia = new LatLng(-46.603354, -1.8883335);
        mMap.addMarker(new MarkerOptions().position(francia).title("Estoy en francia").snippet("Es mi lugar favorito").draggable(true)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.world)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(francia));

    }

    /**
     * Al hacer click en el Mapa
     * @param latLng
     */
    @Override
    public void onMapClick(LatLng latLng) {
        mMap.addMarker(new MarkerOptions()
            .position(latLng)
                .title("Nuevo Marker")
                .draggable(true)
        );


        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
    marker.hideInfoWindow();
    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        LatLng posicion = marker.getPosition();
    marker.setSnippet(posicion.latitude+","+posicion.longitude);
    marker.showInfoWindow();
    }
}
