package com.parra.markergooglemaps;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng peru = new LatLng(-11.999476247483813, -77.06244528293611);
        LatLng españa = new LatLng(41.75786995, 2.03118203615985);
        LatLng japon = new LatLng(35.6828387, 139.7594549);

        MyMarker myMarker = new MyMarker(this);
        mMap.addMarker(myMarker.obtenerMarker(españa,"Estoy en españa",R.drawable.barcelona,"Lugar algo atractivo",true));
        mMap.addMarker(myMarker.obtenerMarker(peru,"Estoy en perú",R.drawable.satanic,"Excelente lugar turistico",true));
        mMap.addMarker(myMarker.obtenerMarker(japon,"Estoy en Japon",R.drawable.japan,"Excelente lugar turistico",true));

/*
        mMap.addMarker(new MarkerOptions()
                .position(peru)
                .title("Estoy en Perú").draggable(true)
        .snippet("Lugar muy atractivo")
        .rotation(0f)
                .alpha(0.5f)
        .visible(true)
        .icon(BitmapDescriptorFactory.fromResource(R.drawable.satanic)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(peru));

*/
    }
}
