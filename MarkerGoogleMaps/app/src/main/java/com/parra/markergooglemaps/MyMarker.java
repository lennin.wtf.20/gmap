package com.parra.markergooglemaps;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MyMarker {
    Context context;

    public MyMarker(Context context) {
        this.context = context;
    }
    public MarkerOptions obtenerMarker(LatLng latLng,String title,int idImg, String description, boolean visible){
        /*
        posicion del marker en el mapa 8latitud,longitud)
        *  .position(peru)
        Titulo del marker
                .title("Estoy en Perú")
                El usuario podra mover el marker
                .draggable(true)
         Agregar descripcion breve al marker
        .snippet("Lugar muy atractivo")

        Permite rotar el marker
        .rotation(90f)

        Agregar transparencia (opacidad) al marker
                .alpha(0.5f)
          Visivilidad del Marker
        .visible(true)

        Cambia el icono por defecto del marker
        .ico
        * */
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng)
                    .title(title)
                    .icon(corrigeSizeIcon(idImg))
                    .snippet(description)
                    .visible(visible);
            return markerOptions;
    }

    private BitmapDescriptor corrigeSizeIcon(int idImg) {
        int alto=60;
        int ancho=60;
        BitmapDrawable bitmapDrawable = (BitmapDrawable) ContextCompat.getDrawable(context, idImg);
        Bitmap bitmap = bitmapDrawable.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(bitmap,ancho,alto,false);
        return BitmapDescriptorFactory.fromBitmap(smallMarker);
    }


}
