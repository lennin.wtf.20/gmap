package com.parra.googlepuntodeleinteres;

import androidx.fragment.app.FragmentActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PointOfInterest;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnPoiClickListener {

    public static final String TAG = MapsActivity.class.getSimpleName();
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnPoiClickListener(this);

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-12.059497553671465, -77.03544080257417);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Usted esta Aqui"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

    }
    private void showDialg(PointOfInterest point){
     Log.d(TAG,">>>Metodo showDialog") ;
        AlertDialog.Builder msgDialog = new AlertDialog.Builder(this);
        msgDialog.setTitle("Punto de Interes");
        msgDialog.setMessage("ID:"+point.placeId+"\nNombre:"+point.name+"\nLatitud:"+point.latLng.latitude+"\nLongitud:"+point.latLng.longitude);
        msgDialog.setCancelable(false);

        msgDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MapsActivity.this,"salir",Toast.LENGTH_SHORT).show();
            }
        });
        //para que salga el mensaje
        msgDialog.show();
    }

    @Override
    public void onPoiClick(PointOfInterest point) {
        if(point!=null){
            showDialg(point);
        }
    }
}
